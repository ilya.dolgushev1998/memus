package com.social.solution.feature_feed_api

import androidx.lifecycle.MutableLiveData

interface FeedFeatureApi {
    fun feedStarter(): FeedStarter
    fun isAuthorized(): MutableLiveData<Boolean>
}