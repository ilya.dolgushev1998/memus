package com.social.solution.memus

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.morozov.core_backend_api.FeatureBackendApi
import com.morozov.lib_backend.LibBackendDependency
import com.social.solution.feature_auth_api.FeatureAuthorizationCallback
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_impl.LibFeedDependency
import com.social.solution.lib_auth.LibAuthDependency

class MainActivity : AppCompatActivity() {

    private lateinit var featureBackendApi: FeatureBackendApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        featureBackendApi = LibBackendDependency.featureBackendApi(applicationContext)
        val featureAuthApi = LibAuthDependency.featureAuthApi(applicationContext, featureBackendApi)
        supportActionBar?.hide()
        actionBar?.hide()
        featureAuthApi.isAuthorized().observe(this, Observer { isGranted ->
            Log.i("MAIN_ACTIVITY", "isGranted1 = $isGranted")
            if (isGranted == null || !isGranted) {
                featureAuthApi.authorizationStarter()
                    .start(
                        this.supportFragmentManager,
                        R.id.main_container,
                        true,
                        AuthCallback(),
                        featureBackendApi
                    )
            } else {
                this.startGrantedApp()
            }
        })
    }

    private fun startGrantedApp() {
        val featureFeedApi =
            LibFeedDependency.featureFeedApi(applicationContext, featureBackendApi)
        featureFeedApi.feedStarter().start(
            supportFragmentManager,
            R.id.main_container,
            true,
            FeedCallback(),
            featureBackendApi
        )
    }

    inner class AuthCallback : FeatureAuthorizationCallback {

        override fun onAuthorized(isRegister: Boolean) {
            if (isRegister) {
                startGrantedApp()
            } else {
                Toast.makeText(applicationContext, "Warn", Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class FeedCallback : FeatureFeedCallback {
        override fun foo(some: Boolean) {
            if (some) {
                Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(applicationContext, "Warn", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

