package com.social.solution.feature_feed_impl.ui.adapters

import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater.from
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.models.Post


class FeedPostAdapter(val posts: List<Post>) :
    RecyclerView.Adapter<FeedPostAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View = from(parent.context).inflate(R.layout.feed_card_post, parent, false)
        return MyViewHolder(
            view
        )
    }

    override fun getItemCount() = 10

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.onBind(position, posts)
    }


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img = view.findViewById<ImageView>(R.id.icon_image)
        val imgPost = view.findViewById<ImageView>(R.id.post_image)
        val name = view.findViewById<TextView>(R.id.name_user)
        fun onBind(
            position: Int,
            posts: List<Post>
        ) {
            val post = posts[position]
            name.setText(post.name + " $position")
        }
    }
}