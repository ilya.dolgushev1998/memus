package com.social.solution.feature_feed_impl.models

data class Collection(
    var drawableResource: Int,
    var name: String,
    var description: String?,
    var isSelected: Boolean
)