package com.social.solution.feature_feed_impl.models

data class Category(val name: String, var isChecked: Boolean)