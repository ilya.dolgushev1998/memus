package com.social.solution.feature_feed_impl.ui

import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedCallback


object MainObject {
    
    var mCallback: FeatureFeedCallback? = null
    var mBackendApi: FeatureBackendApi? = null
}