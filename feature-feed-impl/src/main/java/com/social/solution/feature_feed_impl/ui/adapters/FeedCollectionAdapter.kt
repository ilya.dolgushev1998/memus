package com.social.solution.feature_feed_impl.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.models.Collection


class FeedCollectionAdapter(
    private val context: Context,
    private val collection: List<Collection>,
    private val setCollection: (Collection) -> Unit
) : RecyclerView.Adapter<FeedCollectionAdapter.CollectionViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CollectionViewHolder {

        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.feed_card_colection, parent, false)
        return CollectionViewHolder(
            view
        )
    }

    override fun getItemCount() = collection.size

    override fun onBindViewHolder(holder: CollectionViewHolder, position: Int) {
        holder.onBind(position, collection, context, setCollection)
    }

    class CollectionViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val materialCheckBox =
            view.findViewById<ImageView>(R.id.feed_collection_icon)
        private val nameCollection = view.findViewById<TextView>(R.id.feed_card_collection_name)
        private val descriptionCollection =
            view.findViewById<TextView>(R.id.feed_card_collection_description)
        private val collectionFeed = view.findViewById<ConstraintLayout>(R.id.feed_collection)

        fun onBind(
            position: Int,
            collectionList: List<Collection>,
            context: Context,
            setCollection: (Collection) -> Unit
        ) {
            if (position == collectionList.size - 1) {
                val paddingDP = 15 // 15 dps
                val paddingBottomDP = 24 // 24 dps

                val scale: Float = context.resources.displayMetrics.density
                val paddingPX = (paddingDP * scale + 0.5f).toInt()
                val paddingBottomPX = (paddingBottomDP * scale + 0.5f).toInt()
                view.setPadding(paddingPX,paddingPX,paddingPX,paddingBottomPX)
                view.requestLayout()
            }

            val collection = collectionList[position]
            materialCheckBox.setImageDrawable(context.getDrawable(collection.drawableResource))
            nameCollection.text = collection.name
            if (collection.description != null) {
                descriptionCollection.visibility = View.VISIBLE
                descriptionCollection.text = collection.description
            } else descriptionCollection.visibility = View.GONE
            if (collection.isSelected) {
                materialCheckBox.background =
                    context.getDrawable(R.drawable.feed_collection_icon_background_selected)
                materialCheckBox.setColorFilter(context.resources.getColor(R.color.white))
                nameCollection.setTextColor(context.resources.getColor(R.color.colorPrimary))
            } else {
                materialCheckBox.background =
                    context.getDrawable(R.drawable.feed_collection_icon_background_unselected)
                materialCheckBox.setColorFilter(context.resources.getColor(R.color.vulcan))
                nameCollection.setTextColor(context.resources.getColor(R.color.vulcan))
            }
            collectionFeed.setOnClickListener {
                for (i in collectionList.indices) {
                    collectionList[i].isSelected = collectionList[i].name == collection.name
                }
                setCollection(collection)

            }
        }
    }
}
