package com.social.solution.feature_feed_impl.start

import androidx.fragment.app.FragmentManager
import androidx.navigation.fragment.NavHostFragment
import com.morozov.core_backend_api.FeatureBackendApi
import com.social.solution.feature_feed_api.FeatureFeedCallback
import com.social.solution.feature_feed_api.FeedStarter
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.ui.MainObject

class FeedStarterImpl : FeedStarter {

    override fun start(
        manager: FragmentManager, container: Int, addToBackStack: Boolean,
        callback: FeatureFeedCallback, backendApi: FeatureBackendApi
    ) {
        MainObject.mCallback = callback
        MainObject.mBackendApi = backendApi
        val finalHost = NavHostFragment.create(R.navigation.nav_host_feed)
        manager.beginTransaction()
            .replace(container, finalHost)
            .setPrimaryNavigationFragment(finalHost)
            .commitAllowingStateLoss()
    }
}