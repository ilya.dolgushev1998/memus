package com.social.solution.feature_feed_impl.models

import android.graphics.drawable.Drawable

data class Post(
    val name: String,
    val date: String,
    val avatar: Drawable,
    val post: Drawable,
    val countLike: Int,
    val countComment: Int
)