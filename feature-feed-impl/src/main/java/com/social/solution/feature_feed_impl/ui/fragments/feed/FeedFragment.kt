package com.social.solution.feature_feed_impl.ui.fragments.feed

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.social.solution.feature_feed_impl.R
import com.social.solution.feature_feed_impl.ui.adapters.FeedCategoryAdapter
import com.social.solution.feature_feed_impl.ui.adapters.FeedPostAdapter
import com.social.solution.feature_feed_impl.models.Category
import com.social.solution.feature_feed_impl.models.Collection
import com.social.solution.feature_feed_impl.models.Post
import com.social.solution.feature_feed_impl.ui.adapters.FeedCollectionAdapter
import kotlinx.android.synthetic.main.feed_fragment.*


class FeedFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.feed_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.initCollection()
        this.initCategories()
        this.initFeed()
    }

    private fun initCollection() {
        val collections = listOf(
            Collection(
                R.drawable.feed_collection_icon_best_memes,
                "Best memes",
                "Update 1.01.10",
                true
            ),
            Collection(
                R.drawable.feed_collection_icon_collective,
                "Collective",
                "All memes feed",
                false
            ),
            Collection(R.drawable.feed_collection_icon_favourite, "Favourite", null, false)
        )
        val adapterCollection =
            FeedCollectionAdapter(
                context!!,
                collections
            ) { collection ->
                feed_toolbar.title = collection.name
                feed_recycler_view_collection.visibility = View.GONE
            }
        feed_recycler_view_collection.adapter = adapterCollection
        feed_toolbar.setOnClickListener {
            if (feed_recycler_view_collection.visibility == View.VISIBLE) {
                feed_recycler_view_collection.visibility = View.GONE
            } else {
                adapterCollection.notifyDataSetChanged()
                feed_recycler_view_collection.visibility = View.VISIBLE
            }
        }
        Log.i("TEST_CLICK", "aaa")
        feed_recycler_view_collection.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_UP) {
                feed_recycler_view_collection.visibility = View.GONE
            }
            return@setOnTouchListener true
        }
    }

    private fun initCategories() {
        val category = resources.getStringArray(R.array.category_names)
        val categoryList = category.map { Category(it, false) }
        feed_category_list.adapter =
            FeedCategoryAdapter(
                categoryList
            )
        val categoryBetweenItemsDecorator =
            DividerItemDecoration(context, DividerItemDecoration.HORIZONTAL)
        categoryBetweenItemsDecorator.setDrawable(
            ContextCompat.getDrawable(
                context!!,
                R.drawable.feed_category_recycler_view_decorator
            )!!
        )
        feed_category_list.addItemDecoration(categoryBetweenItemsDecorator)
    }

    private fun initFeed() {
        val post = Post(
            "Pepe_master",
            "1 day ago",
            getDrawable(context!!, R.drawable.post_icon_test)!!,
            getDrawable(context!!, R.drawable.post_img_test)!!,
            100,
            16
        )
        val posts =
            listOf(post, post, post, post, post, post, post, post, post, post, post, post, post)

        feed_recycler_view_post.adapter = FeedPostAdapter(posts)
        val postBetweenItemsDecorator =
            DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        postBetweenItemsDecorator.setDrawable(
            ContextCompat.getDrawable(
                context!!,
                R.drawable.feed_post_recycler_view_decorator
            )!!
        )
        feed_recycler_view_post.addItemDecoration(postBetweenItemsDecorator)
    }
}